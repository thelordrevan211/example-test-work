import "react-native-gesture-handler";
import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import IndexRoute from "./src/routers/index.route";
import FormRoute from "./src/routers/form.route";
import ViewRoute from "./src/routers/view.route";

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={IndexRoute}
          options={{ title: "Список" }}
        />
        <Stack.Screen
          name="View"
          component={ViewRoute}
          options={{ title: "Просмотр" }}
        />
        <Stack.Screen
          name="Form"
          component={FormRoute}
          options={{ title: "Добавить" }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
