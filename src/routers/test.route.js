import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, ThemeProvider, ListItem } from 'react-native-elements';
import axios from 'axios';



export default function TestRoute() {
  const [list, setList] = useState(null);

  useEffect(() => {
    axios.get(`https://laborout.dnl.su/api/data`).then(resp => {
      setList(resp.data.data)
    }).catch(error => {
      console.log(error)
    })
  });

  return (
    <View style={styles.container}>
        <Button>11111111111111</Button>
        {list &&
          list.map((l, i) => (
            <ListItem key={i} bottomDivider>
              <ListItem.Content>
                <ListItem.Title>{l.name}</ListItem.Title>
                <ListItem.Subtitle>{l.description} ({l.latitude} - {l.longitude})</ListItem.Subtitle>
              </ListItem.Content>
              <ListItem.Chevron />
            </ListItem>
          ))
        }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    //backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
  },
});
