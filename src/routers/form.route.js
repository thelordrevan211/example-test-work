import React, { useState } from "react";
import { ScrollView, SafeAreaView, View } from "react-native";
import { Button, Input, ListItem } from "react-native-elements";

import axios from "axios";

import styles from "../styles/style";

export default function FormRoute({ route, navigation }) {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [longitude, setLongitude] = useState('');
  const [latitude, setLatitude] = useState('');

  const [loadingButton, setLoadingButton] = useState(false);

  const addItem = () => {
    setLoadingButton(true);
    axios
      .post(`https://laborout.dnl.su/api/data/create`, {
        name,
        description,
        longitude,
        latitude
      })
      .then((resp) => {
        
        console.log(resp.data)
        navigation.navigate("Home", {
          rerender: true,
        });
        setLoadingButton(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scroll}>
        
        <ListItem bottomDivider>
          <ListItem.Content>
            <Input value={name} placeholder="Название" onChangeText={value => setName(value)} />
            <Input value={description} placeholder="Описание" onChangeText={value => setDescription(value)} />
            <Input value={longitude} placeholder="Longitude" onChangeText={value => setLongitude(value)} />
            <Input value={latitude} placeholder="Latitude" onChangeText={value => setLatitude(value)} />
          </ListItem.Content>
        </ListItem>

        

        <View style={styles.btngroup}>
          <Button
            title="Добавить"
            onPress={() => addItem()}
            type="outline"
            loading={loadingButton}
            buttonStyle={styles.btn}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
