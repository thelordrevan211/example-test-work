import React, { useState, useEffect } from "react";
import { ScrollView, SafeAreaView, View } from "react-native";
import { Button, ListItem } from "react-native-elements";

import axios from "axios";

import styles from "../styles/style";



export default function ViewRoute({ route, navigation }) {
  const [item, setItem] = useState(null);
  const [loadingButton, setLoadingButton] = useState(false);

  const removeItem = (itemId) => {
    setLoadingButton(true);
    axios
      .delete(`https://laborout.dnl.su/api/data/delete?id=${itemId}`)
      .then((resp) => {
        navigation.navigate('Home', {
          rerender: true
        })
        setLoadingButton(false)
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    setItem({
      id: route.params.id || "Неизвестно",
      name: route.params.name || "Неизвестно",
      coords:
        `(${route.params.latitude} - ${route.params.longitude})` ||
        "Неизвестно",
      description: route.params.description || "Неизвестно",
      dateStart: route.params.created || "Неизвестно",
      dateUpdate: route.params.updated || "Неизвестно",
    });
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      {item && (
        <ScrollView style={styles.scroll}>
          <ListItem bottomDivider>
            <ListItem.Content>
              <ListItem.Title>Название: </ListItem.Title>
              <ListItem.Subtitle>{item.name}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>

          <ListItem bottomDivider>
            <ListItem.Content>
              <ListItem.Title>Координаты: </ListItem.Title>
              <ListItem.Subtitle>{item.coords}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>

          <ListItem bottomDivider>
            <ListItem.Content>
              <ListItem.Title>Описание: </ListItem.Title>
              <ListItem.Subtitle>{item.description}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>

          <ListItem bottomDivider>
            <ListItem.Content>
              <ListItem.Title>Дата создания: </ListItem.Title>
              <ListItem.Subtitle>{item.dateStart}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>

          <ListItem bottomDivider>
            <ListItem.Content>
              <ListItem.Title>Дата обновления: </ListItem.Title>
              <ListItem.Subtitle>{item.dateUpdate}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>

          <View style={styles.btngroup}>
            {/*<Button
            title="Редактировать"
            buttonStyle={styles.btn}
          />*/}

            <Button
              title="Удалить"
              onPress={() => removeItem(item.id)}
              type="outline"
              loading={loadingButton}
              buttonStyle={styles.btn}
            />

            {/*<Button
            title="Назад"
            type="outline"
            buttonStyle={styles.btn}
            onPress={() =>
              navigation.navigate('Home')
            }
          />*/}
          </View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
}
