import React, { useState, useEffect } from "react";
import { ScrollView, SafeAreaView, View } from "react-native";
import { Button, ListItem } from "react-native-elements";
import axios from "axios";

import styles from "../styles/style";

export default function IndexRoute({ route, navigation }) {
  const [list, setList] = useState(null);

  const loadData = () => {
    console.log('loaddata')
    axios
      .get(`https://laborout.dnl.su/api/data`)
      .then((resp) => {
        setList(resp.data.data);
        console.log(resp.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    console.log('useEffect')
    loadData();
  }, []);

  if(route.params && route.params.rerender) {
    console.log(1)
    loadData();
    route.params = undefined;
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scroll}>
        {list &&
          list.map((l, i) => (
            <ListItem
              key={i}
              bottomDivider
              onPress={() => navigation.navigate("View", l)}
            >
              <ListItem.Content>
                <ListItem.Title>{l.name}</ListItem.Title>
                <ListItem.Subtitle>
                  {l.description} ({l.latitude} - {l.longitude})
                </ListItem.Subtitle>
              </ListItem.Content>
              <ListItem.Chevron />
            </ListItem>
          ))}

        <View style={styles.btngroup}>
          <Button title="Добавить" type="outline" buttonStyle={styles.btn} onPress={() => navigation.navigate("Form")} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
