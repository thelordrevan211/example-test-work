import { StyleSheet } from 'react-native'

import Constants from 'expo-constants';

const styles = StyleSheet.create({
  scroll: {
    
  },
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  btngroup: {
    marginTop: 30,
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
  },
  btn: {
    marginTop: 10,
    height: 50,
  }
});

export default styles;
